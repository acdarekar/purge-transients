<?php
/**
 * Plugin Name: Purge Transients
 * Version: 0.1-alpha
 * Description: Deletes expired transients. Cron based.
 * Author: Akshay Darekar
 * Author URI: http://about.me/acdarekar
 * Plugin URI: https://bitbucket.org/acdarekar/
 * Text Domain: purge-transients
 * Domain Path: /languages
 * @package Purge-transients
 */

// TODO:
// default settings
// get settings
// languages

/**
* Base Class for Purge Transients Plugin
*/
class PurgeTransients
{
    public static $pt_settings;
    function __construct() {
        // Add admin settings page
        add_action('admin_init', array($this, 'pt_settings_init'));
        add_action('admin_menu', array($this, 'pt_settings_page_menu'));

        // add activation hooks
        add_action('purge_transients_cron_job', array($this, 'set_cron_job'));
        register_activation_hook(__FILE__, array( $this, 'pt_activation'));
        register_deactivation_hook(__FILE__, array( $this, 'pt_deactivation'));
    }

    public function pt_activation() {
        if (!wp_next_scheduled('purge_transients_cron_job')) {
            wp_schedule_event(time(), 'daily', 'purge_transients_cron_job');
        }
        add_action('purge_transients_cron_job', array($this, 'set_cron_job'));
    }

    public function pt_deactivation() {
        wp_clear_scheduled_hook('purge_transients_cron_job');
    }

    public function set_cron_job() {
    	$this->purge_transients();
    }

    public function pt_settings_page_menu() {
        add_options_page('Purge Transients', 'Purge Transients', 'manage_options', 'purge_transients_settings', array($this, 'pt_settings_render_page'));
    }

    public function pt_settings_render_page() {
        ?>
        	<form action='options.php' method='post'>
        		<h1>Purge Transients</h1>
        		<?php
        		settings_fields( 'pt_settings_page' );
        		do_settings_sections( 'pt_settings_page' );
        		submit_button();
        		?>
        	</form>
    	<?php
    	$this->purge_transients();
    }

    public function pt_settings_init() {
		register_setting( 'pt_settings_page', 'pt_settings' );

		add_settings_section(
			'pt_pluginPage_section',
			__( 'Plugin Settings', 'purge-transients' ),
			array($this, 'pt_settings_section_callback'),
			'pt_settings_page'
		);

		add_settings_field(
			'pt_transient_age',
			__( 'Age of transients to be deleted:', 'purge-transients' ),
			array($this, 'pt_transient_age_render'),
			'pt_settings_page',
			'pt_pluginPage_section'
		);

		add_settings_field(
			'pt_transient_limit',
			__( 'No. of transients to be deleted:', 'purge-transients' ),
			array($this, 'pt_transient_limit_render'),
			'pt_settings_page',
			'pt_pluginPage_section'
		);

		add_settings_field(
			'pt_safemode',
			__( 'Safe Mode:', 'purge-transients' ),
			array($this, 'pt_safemode_render'),
			'pt_settings_page',
			'pt_pluginPage_section'
		);
	}

	public function pt_transient_age_render() {
		$settings = get_option( 'pt_settings' );
		?>
		<select name='pt_settings[pt_transient_age]'>
			<option value='7 days' <?php selected( $settings['pt_transient_age'], 2 ); ?>>Older than 7 days</option>
			<option value='1 day' <?php selected( $settings['pt_transient_age'], 2 ); ?>>Older than a day</option>
			<option value='now' <?php selected( $settings['pt_transient_age'], 1 ); ?>>Older than now</option>
		</select>
	<?php
		echo __( '<br /><br />All the transients older than this will be deleted.', 'purge-transients' );
	}

	public function pt_transient_limit_render() {
		$settings = get_option( 'pt_settings' );
		?>
		<select name='pt_settings[pt_transient_limit]'>
			<option value='100' <?php selected( $settings['pt_transient_limit'], 2 ); ?>>100</option>
			<option value='1000' <?php selected( $settings['pt_transient_limit'], 2 ); ?>>1000</option>
			<option value='10000' <?php selected( $settings['pt_transient_limit'], 1 ); ?>>10000</option>
		</select>
	<?php
		echo __( '<br /><br />All the transients older than this will be deleted.', 'purge-transients' );
	}

	public function pt_safemode_render() {
		$settings = get_option( 'pt_settings' );
		?>
		<input type='checkbox' checked name='pt_settings[pt_safemode]' <?php checked( $settings['pt_safemode'], 1 ); ?> value='1'>
		<?php
		echo __( '<br /><br />Important! If unchecked, transients will be deleted from database directly.', 'purge-transients' );
	}

	public function pt_settings_section_callback() {
		echo __( 'This section description', 'purge-transients' );
	}

    public function get_transients($older_than='7 days') {
        global $wpdb;

        $older_than_time = strtotime('-' . $older_than);

        if ($older_than_time > time() || $older_than_time < 0) {
            return false;
        }

        $transients = $wpdb->get_col(
            $wpdb->prepare( "
                SELECT REPLACE(option_name, '_transient_timeout_', '') AS transient_name
                FROM {$wpdb->options}
                WHERE option_name LIKE '\_transient\_timeout\__%%'
                AND option_value < %s LIMIT 10000",
                 $older_than_time)
        );
        return $transients;
    }

    public function purge_transients($safemode = true) {
        if ($safemode) {
            foreach ($this->get_transients('7 days') as $transient) {
                get_transient($transient);
            }
        }
    }
}

new PurgeTransients();
